from django.shortcuts import render

from django.http import *
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages

def loginuser(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST.get('email')

        if email is not None:

            try:
                u =   User.objects.create_user(username, email , password)
                u.save()
            except:
                return HttpResponse("error guardando :(")
            else:
                #TODO: Mostrar usuario logueado
                messages.add_message(request, messages.INFO, 'Usuario Creado Con éxito')
        else:
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/main/')
            else:
                messages.add_message(request, messages.INFO, 'Inténtalo de nuevo')        
    return render(request,'camsite/login.html', context_instance=RequestContext(request))

def newuser(request):
    return render(request,'camsite/register.html', context_instance=RequestContext(request))

def logout_view(request):
    logout(request)
    return render(request,'camsite/login.html', context_instance=RequestContext(request))

@login_required(login_url='/login/')
def main(request):
    return render(request,'camsite/conference.html', context_instance=RequestContext(request))
